package com.example.basicauth;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/nonAuth")
public class NonAuthController {

    @GetMapping("/api")
    public String sayHi (){
        return "Hi web!";
    }
}
