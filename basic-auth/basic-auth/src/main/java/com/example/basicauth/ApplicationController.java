package com.example.basicauth;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class ApplicationController {

    @GetMapping("/welcome")
    public String greeting() {
        return "Hi Spring Security!";
    }
}
